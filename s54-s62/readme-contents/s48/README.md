# Session Objectives

At the end of the session, the students are expected to:

- learn how to mimic page navigation by applying client-side routing; and
- show components only when a certain condition is met by applying conditional rendering.

# Resources

## Instructional Materials

- GitLab Repository
- Google Slide Presentation

## Supplemental Materials

- [Conditional Rendering (React.js Docs)](https://reactjs.org/docs/conditional-rendering.html)
- [React Router DOM](https://reactrouter.com/web/guides/quick-start)

# Lesson Proper

## Routing

So far, we have created components for the home page (root URL). However, what if we want to create components for another page? How can we go from one set of components to another?

This is where React Routing comes in. It is a library that keeps our UI components in sync with the URL shown in the browser.

## Conditional Rendering

Sometimes, we also need to show components only when a given condition has been met (e.g. show action buttons for users currently logged in).

To do that, we will need to apply conditional rendering within our components.

# Code Discussion

## Installing the `react-router-dom` Package

Before we proceed with our React.js code, add the mentioned package to our project by executing the command below:

```
npm install react-router-dom
```

## Using the React Router DOM

To start using the package, go to **src/index.js** and add the following code:

```jsx
// Base Imports
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'index.css';

// App Components
import AppNavbar from 'components/AppNavbar';

// Page Components
import Home from 'pages/Home';
import Courses from 'pages/Courses';
import Register from 'pages/Register';
import Login from 'pages/Login';

ReactDOM.render(
    <BrowserRouter>
        <AppNavbar/>
        <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/courses" component={Courses}/>
            <Route exact path="/register" component={Register}/>
            <Route exact path="/login" component={Login}/>
        </Switch>
    </BrowserRouter>, 
    document.getElementById('root')
);
```

The `BrowserRouter` component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser.

The `Switch` component then declares with `Route` we can go to. For example, when the `/courses` is visited in the web browser, React.js will show the `Courses` component to us.

Remove the `Course` component in the return statement of `pages/Home.js` if the component  has a "TypeError: Cannot read property 'name' of undefined" error.

Try navigating to the routes by changing the URL in the web browser.

## Using NavLink in AppNavbar

To make use of the added navigation capabilities in our React.js project, we can use NavLink to provide links with the ability to navigate to one of the declared routes without reloading the page.

Go to **components/AppNavbar.js** and add the following code:

```jsx
// Base Imports
import React from 'react';
import { Link, NavLink } from 'react-router-dom';

// Bootstrap Components
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

export default function AppNavbar() {
    return (
        <Navbar bg="light" expand="lg">
            <Navbar.Brand as={Link} to="/">Zuitt Booking</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                    <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}
```

Demonstrate the added code by using the links in the Navbar. Help the students notice that the browser tab does not have a page loading icon when the link is clicked.

## Conditional Rendering in Navbar

### Preparing the App Component

Let's simulate a login procedure that will change the shown Navbar contents depending on whether a user is logged in or not.

Since we need to add states in our root component, let's create a file named **App.js** inside the **src** folder and add the following code:

```jsx
// Base Imports
import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'index.css';

// App Components
import AppNavbar from 'components/AppNavbar';

// Page Components
import Home from 'pages/Home';
import Courses from 'pages/Courses';
import Register from 'pages/Register';
import Login from 'pages/Login';

export default function App() {
    return (
        <BrowserRouter>
            <AppNavbar/>
            <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/courses" component={Courses}/>
                <Route exact path="/register" component={Register}/>
                <Route exact path="/login" component={Login}/>
            </Switch>
        </BrowserRouter>
    )
}
```

Then change the contents of **src/index.js** into the following code:

```jsx
// Base Imports
import React from 'react';
import ReactDOM from 'react-dom';
import App from 'App';

ReactDOM.render(<App/>, document.getElementById('root'));
```

The App component will now serve as our root component that gets rendered into the web page.

### Adding User State in App Component

Go to **src/App.js** and add the following code:

```jsx
// Base Imports
import React, { useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'index.css';

// App Components
import AppNavbar from 'components/AppNavbar';

// Page Components
import Home from 'pages/Home';
import Courses from 'pages/Courses';
import Register from 'pages/Register';
import Login from 'pages/Login';

export default function App() {
    const [user, setUser] = useState(null);

    return (
        <BrowserRouter>
            <AppNavbar user={user}/>
            <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/courses" component={Courses}/>
                <Route exact path="/register" component={Register}/>
                <Route exact path="/login" component={Login}/>
            </Switch>
        </BrowserRouter>
    )
}
```

The App component will manage the user state and the state is passed as a prop to the AppNavbar component.

To use the passed prop, go to **src/components/AppNavbar.js** and add the following code:

```jsx
// Base Imports
import React, { Fragment } from 'react';
import { Link, NavLink } from 'react-router-dom';

// Bootstrap Components
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

export default function AppNavbar({ user }) {
    let rightNav = (user === null) ? (
        <Fragment>
            <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
            <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
        </Fragment>
    ) : (
        <Fragment>
            <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
        </Fragment>
    );

    return (
        <Navbar bg="light" expand="lg">
            <Navbar.Brand as={Link} to="/">Zuitt Booking</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                    <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
                </Nav>
                <Nav className="ml-auto">
                    {rightNav}
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}
```

# Activity

## Instructions

Using the code provided:

```jsx
// Base Imports
import React from 'react';
import { Link } from 'react-router-dom';

// Bootstrap Components
import Container from 'react-bootstrap/Container';

export default function NotFound() {
    return (
        <Container fluid>
            <h3>Page Not Found</h3>
            <p>Go back to the <Link to="/">homepage</Link>.</p>
        </Container>
    )
}
```

When the user goes to a route that is not included in the Switch component, it should display the NotFound page component.

## Expected Output

![readme-images/Untitled.png](readme-images/Untitled.png)

## Solution

**Not Found**

```jsx
// Base Imports
import React, { useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'index.css';

// App Components
import AppNavbar from 'components/AppNavbar';

// Page Components
import Home from 'pages/Home';
import Courses from 'pages/Courses';
import Register from 'pages/Register';
import Login from 'pages/Login';
import NotFound from 'pages/NotFound';

export default function App() {
    const [user, setUser] = useState(null);

    return (
        <BrowserRouter>
            <AppNavbar user={user}/>
            <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/courses" component={Courses}/>
                <Route exact path="/register" component={Register}/>
                <Route exact path="/login" component={Login}/>
                <Route component={NotFound}/>
            </Switch>
        </BrowserRouter>
    )
}
```