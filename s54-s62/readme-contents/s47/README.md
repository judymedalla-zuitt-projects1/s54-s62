# Session Objectives

At the end of the session, the students are expected to:

- make a component reactive by applying an effect hook and handling events from elements.

# Resources

## Instructional Materials

- GitLab Repository
- Google Slide Presentation

## Supplemental Materials

- [Using the Effect Hook (React.js Docs)](https://reactjs.org/docs/hooks-effect.html)
- [Handling Events (React.js Docs)](https://reactjs.org/docs/handling-events.html)
- [Forms (React.js Docs)](https://reactjs.org/docs/forms.html)

# Lesson Proper

## Effect Hooks

Effect hooks in React allow us to **execute** a piece of code whenever a **component** gets **rendered** to the page or if the **value** of a state **changes**.

This is especially useful if we want our page to be **reactive**.

Examples of *side effects* (or simply *effects*) are fetching data from a server, doing mathematical computations, or simply changing the contents of a page.

## Handling Events in React

Handling events with React elements is very similar to handling events on DOM elements. There are some syntax differences:

- React events are named using camelCase, rather than lowercase.
- With JSX you pass a function as the event handler, rather than a string.

For example, the HTML:

```html
<button onclick="activateLasers()">Activate Lasers</button>
```

Then, in React:

```jsx
<button onClick={activateLasers}>Activate Lasers</button>
```

# Code Discussion

## Demonstrating the Effect Hook

To demonstrate the useEffect hook, create a file named **Counter.js** inside the **components** folder then add the following code:

```jsx
// Base Imports
import React, { useState } from 'react';

// Bootstrap Components
import Container from 'react-bootstrap/Container';

export default function Counter() {
    const [count, setCount] = useState(0);

    return (
        <Container fluid>
            <p>You clicked {count} times</p>
            <button>Click me</button>
        </Container>
    )
}
```

Then, let's add the useEffect:

```jsx
// Base Imports
import React, { useState, useEffect } from 'react';

// Bootstrap Components
import Container from 'react-bootstrap/Container';

export default function Counter() {
    const [count, setCount] = useState(0);

    // useEffect(function, array);

    useEffect(() => {
        document.title = `You clicked ${count} times`;
    }, [count]);

    return (
        <Container fluid>
            <p>You clicked {count} times</p>
            <button onClick={() => setCount(count + 1)}>Click me</button>
        </Container>
    )
}
```

The useEffect is added in the import statement.

Using the useEffect requires two arguments: a **function** and an **array** of variables.

When the value of a variable in a given array is **changed**, the given function will be **triggered**.

In the case of the code above, when the value of `count` has been changed, the page title will be updated with the number of times the button has been clicked.

Also, in the button component, we see the use of **onClick** event prop. Every time the button is clicked, it uses an **anonymous** function that will update the count state.

The **onClick** is written in camel casing because the keyword is being sent to the component as a prop. The **onclick** (without the capital C) is a reserved keyword in plain JavaScript, along with all the other event identifiers (like onkeyup or onkeypress).

Next, let's update what is shown to the browser by updating the **index.js** file.

```jsx
// Page Components
import Home from 'pages/Home';
import Courses from 'pages/Courses';

// UI Components
import Counter from 'components/Counter';

ReactDOM.render(
    <Fragment>
        <AppNavbar/>
        <Counter/>
    </Fragment>, 
    document.getElementById('root')
);
```

The output should be like this.

![readme-images/s47-count-demonstration.gif](readme-images/s47-count-demonstration.gif)

If the useEffect had an empty array earlier, the page title would only be updated with "You clicked 0 times" and will no longer be updated when the button is clicked.

This means that if a useEffect has an empty array, the function within it will execute only once.

## Updating Course Component with Effect Hook

Open the **Course.js** inside the **components** folder and update the code:

```jsx
// Base Imports
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

// Bootstrap Components
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

export default function Course(props) {
    let course = props.course;

    const [isDisabled, setIsDisabled] = useState(false);
    const [seats, setSeats] = useState(30);

    useEffect(() => {
        if (seats === 0) {
            setIsDisabled(true);
        }
    }, [seats]);

    return (
        <Card>
            <Card.Body>
                <Card.Title>{course.name}</Card.Title>
                <h6>Description</h6>
                <p>{course.description}</p>
                <h6>Price</h6>
                <p>PHP {course.price}</p>
                <h6>Seats</h6>
                <p>{seats} remaining</p>
                <Button variant="primary" onClick={() => setSeats(seats - 1)} disabled={isDisabled}>Enroll</Button>
            </Card.Body>
        </Card>
    )
}
```

Instead of having a separate count state, we added a state to determine whether a button should be disabled or not.

Whenever the seats state is changed, the effect hook checks if the resulting seats state has a value of zero. Once the condition becomes true, the isDisabled state becomes true.

The isDisabled state is then used as a value of the disabled props of the Button component.

Go back to **index.js** and update the following:

```jsx
// Page Components
import Home from 'pages/Home';
import Courses from 'pages/Courses';

ReactDOM.render(
    <Fragment>
        <AppNavbar/>
        <Courses/>
    </Fragment>, 
    document.getElementById('root')
);
```

The output should be like this:

![readme-images/s47-disabled-button.gif](readme-images/s47-disabled-button.gif)

## Creating Register Page Component

Create a file named **Register.js** inside the **pages** folder and add the following.

```jsx
// Base Imports
import React, { useState, useEffect } from 'react';

// Bootstrap Components
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';

export default function Register() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passwordConfirm, setPasswordConfirm] = useState('');
    const [isDisabled, setIsDisabled] = useState(true);

    return (
        <Container fluid>
            <h3>Register</h3>
            <Form>
                <Form.Group>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" required/>
                    <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" required/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control type="password" placeholder="Verify Password" required/>
                </Form.Group>
                <Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
            </Form>
        </Container>
    )
}
```

Then, let's add the event listeners for when the input values change.

```jsx
export default function Register() {
    ...

    return (
        <Container fluid>
            <h3>Register</h3>
            <Form>
                <Form.Group>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" onChange={(e) => setEmail(e.target.value)} required/>
                    <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" onChange={(e) => setPassword(e.target.value)} required/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control type="password" placeholder="Verify Password" onChange={(e) => setPasswordConfirm(e.target.value)} required/>
                </Form.Group>
                <Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
            </Form>
        </Container>
    )
}
```

We can declare multiple useEffect for each and every input state that we have so we can see the values change every key press.

```jsx
export default function Register() {
    ...

    useEffect(() => {
        console.log(email);
    }, [email]);

    useEffect(() => {
        console.log(password);
    }, [password]);

    useEffect(() => {
        console.log(passwordConfirm);
    }, [passwordConfirm]);
    
    return (
        <Container fluid>
            <h3>Register</h3>
            <Form>
                <Form.Group>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" onChange={(e) => setEmail(e.target.value)} required/>
                    <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" onChange={(e) => setPassword(e.target.value)} required/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control type="password" placeholder="Verify Password" onChange={(e) => setPasswordConfirm(e.target.value)} required/>
                </Form.Group>
                <Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
            </Form>
        </Container>
    )
}
```

The output in the browser should be like this.

![readme-images/s47-multiple-effect-hooks.gif](readme-images/s47-multiple-effect-hooks.gif)

To ensure that the value of the input is the same as the value in the input state, we must implement **two-way data binding**.

Two-way data binding happens when both:

- The data we changed in the view has updated the state.
- The data in the state has updated the view.

![readme-images/Untitled.png](readme-images/Untitled.png)

Diagram of a two-way data binding. Source of original diagram in [https://reactgo.com/two-way-data-binding-react/](https://reactgo.com/two-way-data-binding-react/)

Add the highlighted code to implement two-way data binding.

```jsx
export default function Register() {
    ...
    
    return (
        <Container fluid>
            <h3>Register</h3>
            <Form>
                <Form.Group>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required/>
                    <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control type="password" placeholder="Verify Password" value={passwordConfirm} onChange={(e) => setPasswordConfirm(e.target.value)} required/>
                </Form.Group>
                <Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
            </Form>
        </Container>
    )
}
```

Next, add the effect hook for determining whether the submit button will be disabled or not.

```jsx
export default function Register() {
    ...

    useEffect(() => {
        let isEmailNotEmpty = email !== '';
        let isPasswordNotEmpty = password !== '';
        let isPasswordConfirmNotEmpty = passwordConfirm !== '';
        let isPasswordMatched = password === passwordConfirm;

        // Determine if all conditions have been met.
        if (isEmailNotEmpty && isPasswordNotEmpty && isPasswordConfirmNotEmpty && isPasswordMatched) {
            setIsDisabled(false);
        } else {
            setIsDisabled(true);
        }
    }, [email, password, passwordConfirm]);
    
    return (
        <Container fluid>
            <h3>Register</h3>
            <Form>
                <Form.Group>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required/>
                    <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control type="password" placeholder="Verify Password" value={passwordConfirm} onChange={(e) => setPasswordConfirm(e.target.value)} required/>
                </Form.Group>
                <Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
            </Form>
        </Container>
    )
}
```

Finally, add the function that will be executed once the form has been submitted.

```jsx
export default function Register() {
    ...
    
    function register(e) {
        e.preventDefault();
        alert('Registration successful, you may now log in.');

        setEmail('');
        setPassword('');
        setPasswordConfirm('');
    }    

    return (
        <Container fluid>
            <h3>Register</h3>
            <Form onSubmit={register}>
                <Form.Group>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required/>
                    <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control type="password" placeholder="Verify Password" value={passwordConfirm} onChange={(e) => setPasswordConfirm(e.target.value)} required/>
                </Form.Group>
                <Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
            </Form>
        </Container>
    )
}
```

The `e.preventDefault()` is included to prevent the form from reloading the whole page.

If the function to be used in an event handler has a name, we can use the `event={functionName}` format. Parenthesis is excluded since we are only pointing a reference to a named function once the `onSubmit` is triggered.

The output should be like this.

![readme-images/s47-register-page-demo.gif](readme-images/s47-register-page-demo.gif)

# Activity

## Instructions

- Using the codes from the Register page component, create a **Login.js** in the **pages** folder and create a page that simulates the login using an email and password.
- Update the **index.js** so that the **Login** page is shown.

No email and password validation is required from this activity, although you may give it as a stretch goal to the students.

There is no apparent use for effect hooks in this activity, only events and forms. Though it will be used in the upcoming sessions.

## Expected Output

![readme-images/s47-login-demo.gif](readme-images/s47-login-demo.gif)

## Solution

**src/index.js**

```jsx
// Page Components
import Home from 'pages/Home';
import Courses from 'pages/Courses';
import Register from 'pages/Register';
import Login from 'pages/Login';

ReactDOM.render(
    <Fragment>
        <AppNavbar/>
        <Login/>
    </Fragment>, 
    document.getElementById('root')
);
```

**src/pages/Login.js**

```jsx
// Base Imports
import React, { useState } from 'react';

// Bootstrap Components
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';

export default function Login() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    function login(e) {
        e.preventDefault();
        alert('You are now logged in.');

        setEmail('');
        setPassword('');
    }    

    return (
        <Container fluid>
            <h3>Login</h3>
            <Form onSubmit={login}>
                <Form.Group>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
                </Form.Group>
                <Button variant="success" type="submit">Login</Button>
            </Form>
        </Container>
    )
}
```