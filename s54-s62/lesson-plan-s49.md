# WDC028 - S49 - React.js - App State Management

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1ARIFrC-lfpu6SU3kTTFKixTmnxQI2w9GMIBGprOYK2Q/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1ARIFrC-lfpu6SU3kTTFKixTmnxQI2w9GMIBGprOYK2Q/edit#slide=id.g53aad6d9a4_0_745) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/fullstack/s45-s50) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/fullstack/s45-s50/Manual.js) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                               | Link                                                         |
| ----------------------------------- | ------------------------------------------------------------ |
| React JS Context                    | [Link](https://reactjs.org/docs/context.html)                |
| React JS React.createContext Method | [Link](https://reactjs.org/docs/context.html#reactcreatecontext) |
| React JS Context Provider Component | [Link](https://reactjs.org/docs/context.html#contextprovider) |
| React JS useContext Hook            | [Link](https://reactjs.org/docs/hooks-reference.html#usecontext) |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[3 hrs] - React JS Context
		- createContext() method
		- Provider Component
		- useContext() hook
	2.[1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

​	

[Back to top](#table-of-contents)