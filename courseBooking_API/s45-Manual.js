/*
===========================================
S45 - Express.js - API Development (Part 3)
===========================================
Booking System API - Authorization via JWT, Retrieval of User Details
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1IN24Fx5PnrlIq4Bd_UIYwQJg93xqisWRRnH0HuRTrm0/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s32-s36

Other References:
	JSON Web Tokens
		https://jwt.io/
	jsonwebtoken Package
		https://www.npmjs.com/package/jsonwebtoken
	What Is Middleware
		https://www.redhat.com/en/topics/middleware/what-is-middleware
	JavaScript next Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Generator/next
	JavaScript typeof Operator
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof
	Creating Git Projects:
			GitLab
				https://gitlab.com/projects/new#blank_project
			GitHub
				https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
==========
Discussion
========== 
*/

/*
1. Create a "verify" method that will verify the validity of the JWT.
	Application > auth.js
*/

		/*...*/

		module.exports.createAccessToken = (user) => {
			/*...*/
		}

		//[SECTION] Token Verification
		/*
		- Analogy
			Receive the gift and open the lock to verify if the the sender is legitimate and the gift was not tampered with
		*/
		module.exports.verify = (req, res, next) => {
			//middlewares which have access to req,res and next also can send responses to the client.

			// The token is retrieved from the request header
			// This can be provided in postman under
				// Authorization > Bearer Token
			console.log(req.headers.authorization);

			//req.headers.authorization contains sensitive data and especially our token
			let token = req.headers.authorization;


			//IF we are not passing token in our request authorization or in our postman authorization, then here in our api, req.headers.authorization will be undefined.

			//This if statement will first check IF token variable contains undefined or a proper jwt. If it is undefined, we will check token's data type with typeof, then send a message to the client.
			if(typeof token === "undefined"){

				return res.send({ auth: "Failed. No Token" });
			
			} else {

				console.log(token);
				/*
					slice() is a method which can be used on strings and arrays.

					This will allow us to copy a part of the string.

					slice(<startingPosition>,<endPosition>)

					Starting position indicates the index number we will copy from.

					End position indicates the index number we will copy up to but not including the item in that position.
					
					Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxZjBjMzYyNmNhYzJjM2VhYTBmY2I5YSIsImVtYWlsIjoic3BpZGVybWFuM0BnbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjQzMjQ1MTEyfQ.c29qelk9GkrnZP10M6wqo6fiTKHPk-c15DcpSBsKq7I 

					"Peter"
					
					slice(3,string.length)

					"er"

					Essentially, we extracted just the jwt token and re-assigned to our token variable.

				*/
				token = token.slice(7, token.length);

				console.log(token);

				//[SECTION] Token decryption
				/*
				- Analogy
					Open the gift and get the content
				*/

				// Validate the token using the "verify" method decrypting the token using the secret code
				jwt.verify(token, secret, function(err, decodedToken){
					//err will contain the error from decoding your token. This will contain the reason why we will reject the token.
					//IF verification of the token is a success, then jwt.verify will return the decoded token.
					if(err){
						return res.send({
							auth: "Failed",
							message: err.message
						});

					} else {

						console.log(decodedToken);//contains the data from our token
						
						req.user = decodedToken
						//user property will be added to request object and will contain our decodedToken. It can be accessed in the next middleware/controller.

						next();
						//middleware function
						//next() will let us proceed to the next middleware OR controller
					}
				})
			}
		};


		/*
		Important Note
			- Refer to "references" section of this file to find the documentations for JSON Web Tokens and jsonwebtoken Package.
		*/

/*
2. Create a "decode" method that will decrypt the JSON web token to obtain the information found in it.
	Application > auth.js
*/

		/*...*/

		module.exports.verify = (req, res, next) => {
			/*...*/
		}

		//[SECTION] verifyAdmin will also be used a middleware.
		module.exports.verifyAdmin = (req, res, next) => {
		//verifyAdmin comes after the verify middleware,
		//Do we have any access to our user's isAdmin detail?
		//We can get details from req.user because verifyAdmin comes after verify method.
		//Note: You can only have req.user for any middleware or controller that comes after verify.

		//console.log(req.user); //use this console to double check the data.
			if(req.user.isAdmin){
				//if the logged in user, based on his token is an admin, we will proceed to the next middleware/controller
				next();

			} else {

				return res.send({
					auth: "Failed",
					message: "Action Forbidden"
				})
			}
		}

/*
3. Refactor the "/details" route to implement the "verify" method.
	Application > routes > user.js
*/

		/*...*/
		const userController = require("../controllers/user");
		//import the auth.js to user routes so we can use the function for token
		const auth = require('../auth');


		// destructure verify from auth, 
		//or you can use auth.verify/auth.verifyAdmin if you will not destructure it.
		const { verify, verifyAdmin } = auth;

		router.post("/checkEmail", (req, res) => {
			/*...*/
		});

		/*...*/

		router.post("/login", (req, res) => {
			/*...*/
		});

		// The "verity" acts as a middleware to ensure that the user is logged in before they can enroll to a course
		//since we have next() used in verify in auth, the next() will let us proceed to the next function w/c is the userController.getProfile
		router.get("/details", verify, userController.getProfile);

		/*...*/

/*
4. Refactor the "getProfile" function and changed the req.body.id to req.user.id since we already added the verify auth
	Application > controllers > user.js
*/

		//[SECTION] Retrieve user details

			module.exports.getProfile = (req, res) => {
				return User.findById(req.user.id)
				/*...*/
			};



		/*
		Important Note
			- Refer to "references" section of this file to find the documentations for What Is Middleware and JavaScript next Method.
		*/





/*
5. Process a GET request at the "/details" route using postman to retrieve the details of the user.
	Postman
*/

		url: http://localhost:4000/users/details
		method: GET
		headers:
			Type: Bearer Token
			Token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxM2U3N2M5NDIzMTc3YTVhYjlhMDY2ZCIsImVtYWlsIjoiam9obkBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2MzE0ODkyNzV9._8QPUI5S0flEO3pMncEo2bQ_GzTQfzLUM_Z2s8ljAQQ



/*
========
Activity
========
*/

/*
Activity Reference

Route Prefixing/Grouping
https://medium.com/@zahiruldu/nodejs-route-prefixing-in-expressjs-65196adb3167

Postman Auth Tab
https://learning.postman.com/docs/sending-requests/authorization/specifying-authorization-details/

Activity

Member 1:
1. Update your local sessions git repo, push with the commit message, "Add discussion s45"
2. Create a "course.js" inside routes folder to store the routes for our courses. 
3. Use app.use() to group all routes related to courses and add "/courses" as route prefix.

Member 2:
4. Create a "course.js" file inside the controllers folder to store the controller functions that will contain the business logic for our course CRUD operations.
5. Connect the course.js-controllers to course.js-routes
	- Provide the name of the controller variable as "courseController."
	- Import the auth module using require() and destructure the verify and verifyAdmin
	
Member 3
6. Create a POST route for creating a course if it is an admin.
	- Use the verifyAdmin as a middleware to confirm if the user is an admin or not
	- Note: you can't use the verifyAdmin() without verify().
	- Connect the POST route to controller, name the function as "addCourse"

Member 4
7. Create "addCourse" function to create a new course (just like how we add user in User registration.)
	Steps:
		1. Create a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		2. Uses the information from the request body to provide all the necessary information.
		3. Saves the created object to our database.
		4. Returns true for successful creation. Else, returns false.

Member 5:
8. Process a POST request at the "/courses" route using postman to create a new course.
	- Make sure to change a user to admin in MongoDB manually.
	- Login using the admin account and copy the token in Authorization Postman.
	- Make sure to pass a token.
	- Save the postman request as s45-Requests

All members:
9. Check out to your own git branch with git checkout -b <branchName>.
10. Update your local sessions git repository and push to git with the commit message of Add activity code s45.
11. Add the sessions repo link in Boodle for s45.

*/



/*
Solution:

1. Create a "course.js" inside routes folder to store the routes for our courses. Add the following dependencies needed for routes. 

	Application > routes > course.js
*/

	//[SECTION] Dependencies and Modules
		const express = require("express");

	//[SECTION] Routing Component
		const router = express.Router();

		// Allows us to export the "router" object that will be accessed in our "index.js" file
		module.exports = router;

/*
2. Implement the main route for our course routes.
	Application > index.js
*/

		/*...*/
		const userRoutes = require("./routes/user");
		const courseRoutes = require("./routes/course");

		/*...*/
		app.use("/users", userRoutes);
		// Defines the "/courses" string to be included for all course routes defined in the "course" route file
		//http://localhost:4000/courses
		app.use("/courses", courseRoutes);

		if(require.main === module){
			app.listen(process.env.PORT || port, () => {
			    console.log(`API is now online on port ${ process.env.PORT || port }`)
			});
		}

		module.exports = app;
/*
3. Create a "course.js" file inside the controllers folder to store the controller functions that will contain the business logic for our course CRUD operations.
	Application > controllers > course.js
*/

		const Course = require("../models/Course");

/*
4. Connect the course.js-controllers to course.js-routes
	- Provide the name of the controller variable as "courseController."
	- Add the auth and destructure the verify and verifyAdmin
	Application > routes > course.js
*/

		//[SECTION] Dependencies and Modules
			const express = require('express');
			const courseController = require("../controllers/course");
			const auth = require("../auth") 

			const {verify, verifyAdmin} = auth;

		//[SECTION] Routing Component
			const router = express.Router();

			/*...*/


/*
5. Create a POST route for creating a course if it is an admin.
	- Use the verifyAdmin as a middleware to confirm if the user is an admin or not
	- Note: you can't use the verifyAdmin() without verify().
	- Connect the POST route to controller, name the function as "addCourse"
	Application > routes > course.js
*/


	//[SECTION] create a course POST
		router.post("/", verify, verifyAdmin, courseController.addCourse)

/*
6. Create "addCourse" function to create a new course (just like how we add user in User registration.)
	Steps:
		1. Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		2. Uses the information from the request body to provide all the necessary information.
		3. Saves the created object to our database and add a successful validation true/false.

	Application > controllers > course.js
*/


		//[SECTION] Create a new course
		/*
			Steps:
			1. Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
			2. Uses the information from the request body to provide all the necessary information.
			3. Saves the created object to our database and add a successful validation true/false.
		*/
		module.exports.addCourse = (req, res) => {

			// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
			// Uses the information from the request body to provide all the necessary information
			let newCourse = new Course({
				name : req.body.name,
				description : req.body.description,
				price : req.body.price
			});

			// Saves the created object to our database
			return newCourse.save().then((course, error) => {

				// Course creation successful
				if (error) {
					return res.send(false);

				// Course creation failed
				} else {
					return res.send(true);
				}
			})
			.catch(err => res.send(err))
		};

/*

7. Process a POST request at the "/courses" route using postman to create a new course.
	- Make sure to change the "isAdmin": true to MongoDB manually.
	- login the admin email and copy the authentication token in Authorization Postman.
	Postman
*/
		headers:
			Type: Bearer Token
			Token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyOGIyOTA5YzZlNmE3MGFhYmUwNmFkMyIsImVtYWlsIjoiYWRtaW5AbWFpbC5jb20iLCJpc0FkbWluIjp0cnVlLCJpYXQiOjE2ODQ5MTgxNzV9._mwPdLeWUYMIGwIbK8CDlAJPXnFI6RSnJrDL5Mz9lKw
		url: http://localhost:4000/courses
		method: POST
		body: raw + JSON
			{
			    "name" : "HTML",
				"description" : "Learn the basics of programming.",
				"price" : 1000
			}